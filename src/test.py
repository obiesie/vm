print("Hello world")

class TestAdd(object):

	 def __init__(self, value):
	 	self.value = value

	 def __add__(self, other):
	 	if other.value:
	 		return self.value + other.value


a = TestAdd(5)
b = TestAdd(6)

print(a+b)